/**
 * @format
 */

import {AppRegistry} from 'react-native';
import BottomNavigator from './src/components/navigation/BottomNavigator';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => BottomNavigator);
