import React, {useState} from 'react';
import {Text, View, TouchableOpacity, Image, Switch} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import fontWeight from '../theme/fonts';

const Tile = props => {
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);
  return (
    <TouchableOpacity
      style={{width: wp('100%'), alignItems: 'center', marginTop: 20}}
      onPress={() => {
        props.onpress();
      }}>
      <View style={{flexDirection: 'row', width: wp('80%')}}>
        <Image
          style={{width: 32, height: 32}}
          resizeMode="contain"
          source={props.icon}
        />
        <View style={{marginLeft: 10, flex: 1}}>
          <Text
            style={{
              fontSize: 14,
              color: '#25345F',
              fontWeight: '500',
              fontFamily: fontWeight.font_Bold,
            }}>
            {props.title}
          </Text>
          <Text
            style={{
              fontSize: 13,
              color: '#222222',
              marginTop: 10,
              fontFamily: fontWeight.font_Regular,
            }}
            numberOfLines={2}>
            {props.description}
          </Text>
        </View>
        {props.showSwitch ? (
          <View style={{width: 34, height: 20}}>
            <Switch
              trackColor={{false: '#EEEEEE', true: '#01D167'}}
              thumbColor={props.isSwitchEnabled ? 'white' : 'white'}
              onValueChange={toggleSwitch}
              value={isEnabled}
              style={{transform: [{scaleX: 0.7}, {scaleY: 0.7}]}}
            />
          </View>
        ) : null}
      </View>
    </TouchableOpacity>
  );
};

export default Tile;
