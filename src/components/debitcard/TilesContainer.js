import React from 'react';
import {View} from 'react-native';
import Tile from './Tile';

const TilesContainer = props => {
  const onPressTopUpAccount = () => {
    console.log('onPressTopUpAccount');
  };
  const onPressWeeklySpent = () => {
    console.log('props..', props);
    props.navigation.navigate('SpentLimitScreen');
  };
  const onPressActiveAccount = () => {
    console.log('onPressActiveAccount');
  };
  const onPressDeactivateAccount = () => {
    console.log('onPressDeactivateAccount');
  };
  const onPressPreviouslyDeactivated = () => {
    console.log('onPressPreviouslyDeactivated');
  };
  return (
    <View style={{backgroundColor: 'white'}}>
      <Tile
        title={'Top-up account'}
        description={'Deposit your money to your account to use with card'}
        icon={require('../../assets/images/topup.svg')}
        onpress={onPressTopUpAccount}
      />
      <Tile
        title={'Weekly spending limit'}
        description={'your weekly spending limit is S$ 5000'}
        icon={require('../../assets/images/weeklylimit.svg')}
        showSwitch={true}
        isSwitchEnabled={true}
        onpress={onPressWeeklySpent}
      />
      <Tile
        title={'Freeze card'}
        description={'your debit card is currently active'}
        icon={require('../../assets/images/freezecard.svg')}
        showSwitch={true}
        isSwitchEnabled={false}
        onpress={onPressActiveAccount}
      />
      <Tile
        title={'Get a new card'}
        description={'This deactivates your current debit card'}
        icon={require('../../assets/images/newcard.svg')}
        onpress={onPressDeactivateAccount}
      />
      <Tile
        title={'Deactivated cards'}
        description={'Your previously deactivated cards'}
        icon={require('../../assets/images/deactivate.svg')}
        onpress={onPressPreviouslyDeactivated}
      />
    </View>
  );
};

export default TilesContainer;
