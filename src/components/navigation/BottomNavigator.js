import 'react-native-gesture-handler';

import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {Image, StyleSheet} from 'react-native';
import fontWeight from '../theme/fonts';
import colors from '../../components/theme/colors';

import HomeScreen from '../screens/HomeScreen';
import CreditScreen from '../screens/CreditScreen';
import PaymentsScreen from '../screens/PaymentsScreen';
import ProfileScreen from '../screens/ProfileScreen';
import DebitCardScreen from '../screens/DebitCardScreen';
import SpentLimitScreen from '../screens/SpentLimitScreen';
import {Provider} from 'react-redux';
import store from '../../redux/store';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function HomeStack() {
  return (
    <Stack.Navigator
      initialRouteName="HomeScreen"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
    </Stack.Navigator>
  );
}
function DebitCardStack() {
  return (
    <Stack.Navigator
      initialRouteName="DebitCardScreen"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="DebitCardScreen" component={DebitCardScreen} />
      <Stack.Screen name="SpentLimitScreen" component={SpentLimitScreen} />
    </Stack.Navigator>
  );
}
function PaymentsStack() {
  return (
    <Stack.Navigator
      initialRouteName="PaymentsScreen"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="PaymentsScreen" component={PaymentsScreen} />
    </Stack.Navigator>
  );
}
function CreditStack() {
  return (
    <Stack.Navigator
      initialRouteName="CreditScreen"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="CreditScreen" component={CreditScreen} />
    </Stack.Navigator>
  );
}
function ProfileStack() {
  return (
    <Stack.Navigator
      initialRouteName="ProfileScreen"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="ProfileScreen" component={ProfileScreen} />
    </Stack.Navigator>
  );
}

function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Tab.Navigator
          screenOptions={{
            headerShown: false,
            tabBarActiveTintColor: colors.activeTintColor,
            tabBarStyle: {height: 56},
          }}
          initialRouteName="Home"
          tabBarOptions={{
            activeTintColor: colors.activeTintColor,
            labelStyle: styles.labelStyle,
          }}>
          <Tab.Screen
            name="HomeStack"
            component={HomeStack}
            options={{
              tabBarLabel: 'Home',
              tabBarIcon: ({color, size}) => {
                return (
                  <Image
                    style={styles.tabBarIconStyle}
                    resizeMode="contain"
                    source={require('../../assets/images/home.svg')}
                  />
                );
              },
            }}
          />
          <Tab.Screen
            screenOptions={{headerShown: false}}
            name="DebitCardStack"
            component={DebitCardStack}
            options={{
              tabBarLabel: 'Debit Card',
              tabBarIcon: ({color, size}) => {
                return (
                  <Image
                    style={styles.tabBarIconStyle}
                    resizeMode="contain"
                    source={require('../../assets/images/pay.svg')}
                  />
                );
              },
            }}
          />
          <Tab.Screen
            name="PaymentsStack"
            component={PaymentsStack}
            options={{
              tabBarLabel: 'Payments',
              tabBarIcon: ({color, size}) => {
                return (
                  <Image
                    style={styles.tabBarIconStyle}
                    resizeMode="contain"
                    source={require('../../assets/images/payments.svg')}
                  />
                );
              },
            }}
          />
          <Tab.Screen
            name="CreditStack"
            component={CreditStack}
            options={{
              tabBarLabel: 'Credit',
              tabBarIcon: ({color, size}) => {
                return (
                  <Image
                    style={styles.tabBarIconStyle}
                    resizeMode="contain"
                    source={require('../../assets/images/credit.svg')}
                  />
                );
              },
            }}
          />
          <Tab.Screen
            name="ProfileStack"
            component={ProfileStack}
            options={{
              tabBarLabel: 'Profile',
              tabBarIcon: ({color, size}) => {
                return (
                  <Image
                    style={styles.tabBarIconStyle}
                    resizeMode="contain"
                    source={require('../../assets/images/profile.svg')}
                  />
                );
              },
            }}
          />
        </Tab.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
const styles = StyleSheet.create({
  labelStyle: {
    fontSize: 9,
    fontFamily: fontWeight.font_Bold,
    paddingBottom: 5,
  },
  tabStyle: {
    padding: 10,
    fontFamily: fontWeight.font_Bold,
  },
  tabBarIconStyle: {
    width: 24,
    height: 24,
    padding: 5,
  },
});

export default App;
