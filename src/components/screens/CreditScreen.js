import React from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';
import fontWeight from '../theme/fonts';

const CreditScreen = () => {
  return (
    <View style={styles.screenContainer}>
      <Image
        style={styles.imageStyle}
        source={require('../../assets/images/credit.svg')}
      />
      <Text style={styles.textStyle}>Credit Screen</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    backgroundColor: 'white',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageStyle: {
    width: 40,
    height: 40,
  },
  textStyle: {
    fontSize: 16,
    color: '#25345F',
    fontFamily: fontWeight.font_Bold,
    marginTop: 10,
  },
});

export default CreditScreen;
