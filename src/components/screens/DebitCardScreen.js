import React, {useState} from 'react';
import {Text, View, Image, ScrollView, SafeAreaView} from 'react-native';
import fontWeight from '../theme/fonts';
import TilesContainer from '../debitcard/TilesContainer';

import {connect} from 'react-redux';
import firestore from '@react-native-firebase/firestore';
import {saveCardInfo} from '../../redux/actions/CardInfoAction';

const DebitCardScreen = props => {
  console.log('props', props.navigation);

  const onResult = QuerySnapshot => {
    let data = QuerySnapshot.docs[0]._data;
    this.props.saveCardInfo(data);
  };

  const onError = error => {
    console.error(error);
  };
  firestore().collection('billinginfo').onSnapshot(this.onResult, this.onError);
  console.log('this.props.cardinfo', JSON.stringify(this.props.cardinfo));
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <View
        style={{
          width: '100%',
          backgroundColor: '#0C365A',
          paddingLeft: 15,
          paddingBottom: 100,
        }}>
        <Image
          style={{
            width: 25,
            height: 25,
            alignSelf: 'flex-end',
            margin: 24,
          }}
          resizeMode="contain"
          source={require('../../assets/images/logocolor.svg')}
        />
        <Text
          style={{
            color: 'white',
            fontFamily: fontWeight.font_Bold,
            fontSize: 24,
          }}>
          Debit Card
        </Text>
        <Text
          style={{
            color: 'white',
            fontFamily: fontWeight.font_Regular,
            fontSize: 14,
            marginTop: 24,
          }}>
          {' '}
          Available balance{' '}
        </Text>
        <View style={{flexDirection: 'row', marginTop: 10, paddingBottom: 50}}>
          <View
            style={{
              width: 40,
              height: 22,
              borderRadius: 5,
              backgroundColor: '#01D167',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                color: 'white',
                fontFamily: fontWeight.font_Bold,
                fontSize: 12,
              }}>
              S$
            </Text>
          </View>
          <Text
            style={{
              marginLeft: 10,
              color: 'white',
              fontFamily: fontWeight.font_Bold,
              fontSize: 24,
            }}>
            3000
          </Text>
        </View>
      </View>

      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 20}}
        contentInset={{top: 250}}
        style={{
          backgroundColor: 'transparent',
          position: 'absolute',
          bottom: 0,
          height: '100%',
          width: '100%',
        }}>
        <View style={{backgroundColor: 'transparent'}}>
          <View
            style={{
              backgroundColor: 'white',
              borderTopLeftRadius: 30,
              borderTopRightRadius: 30,
            }}>
            <View style={{paddingTop: 210, alignItems: 'center'}}>
              <View
                style={{
                  position: 'absolute',
                  top: -50,
                  width: 366,
                  backgroundColor: '#01D167',
                  borderRadius: 15,
                }}>
                <View
                  style={{
                    position: 'absolute',
                    top: -28,
                    right: 0,
                    flexDirection: 'row',
                    width: 151,
                    height: 34,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: '#FFFFFF',
                    borderRadius: 5,
                  }}>
                  <Image
                    style={{
                      width: 16,
                      height: 16,
                    }}
                    resizeMode="contain"
                    source={require('../../assets/images/hidecardnumber.svg')}
                  />
                  <Text
                    style={{
                      marginLeft: 5,
                      color: '#01D167',
                      fontFamily: fontWeight.font_Bold,
                      fontSize: 12,
                    }}>
                    Hide card number
                  </Text>
                </View>
                <Image
                  style={{
                    width: 74,
                    height: 21,
                    alignSelf: 'flex-end',
                    margin: 24,
                  }}
                  resizeMode="contain"
                  source={require('../../assets/images/aspirelogo.svg')}
                />
                <Text
                  style={{
                    marginLeft: 24,
                    color: 'white',
                    fontFamily: fontWeight.font_Bold,
                    fontSize: 22,
                  }}>
                  Raghavender Reddy
                </Text>
                <Text
                  style={{
                    marginLeft: 24,
                    color: 'white',
                    fontFamily: fontWeight.font_Bold,
                    fontSize: 14,
                    marginTop: 24,
                  }}>
                  5 6 4 7 3 4 1 1 2 4 1 3 2 0 2 0
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    marginLeft: 24,
                    marginTop: 24,
                  }}>
                  <Text
                    style={{
                      color: 'white',
                      fontFamily: fontWeight.font_Bold,
                      fontSize: 13,
                    }}>
                    Thru: 12/20
                  </Text>
                  <Text
                    style={{
                      marginLeft: 32,
                      color: 'white',
                      fontFamily: fontWeight.font_Bold,
                      fontSize: 13,
                    }}>
                    CVV: 456
                  </Text>
                </View>
                <Image
                  style={{
                    width: 59,
                    height: 20,
                    alignSelf: 'flex-end',
                    margin: 24,
                    paddingBottom: 20,
                  }}
                  resizeMode="contain"
                  source={require('../../assets/images/visalogo.svg')}
                />
              </View>
              <View style={{flex: 1, backgroundColor: 'white'}}>
                <TilesContainer navigation={props.navigation} />
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => ({
  saveCardInfo: data => dispatch(saveCardInfo(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DebitCardScreen);
