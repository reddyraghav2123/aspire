import React from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';
import fontWeight from '../theme/fonts';

const PaymentsScreen = () => {
  return (
    <View style={styles.screenContainer}>
      <Image
        style={styles.imageStyle}
        source={require('../../assets/images/payments.svg')}
      />
      <Text style={styles.textStyle}>Payments Screen</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    backgroundColor: 'white',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageStyle: {
    width: 40,
    height: 40,
  },
  textStyle: {
    fontSize: 16,
    color: '#25345F',
    fontFamily: fontWeight.font_Bold,
    marginTop: 10,
  },
});

export default PaymentsScreen;
