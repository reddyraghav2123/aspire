import React, {useState} from 'react';
import {
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  SafeAreaView,
} from 'react-native';
import fontWeight from '../theme/fonts';

const SpentLimitScreen = props => {
  let data = [1, 2, 3, 4, 5];
  const [limit, setLimit] = useState('');

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <View style={{backgroundColor: '#0C365A', flex: 1}}>
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            padding: 20,
            alignItems: 'center',
          }}>
          <TouchableOpacity
            style={{flex: 1}}
            onPress={() => {
              props.navigation.goBack();
            }}>
            <Image
              style={{width: 20, height: 20, alignSelf: 'flex-start'}}
              source={require('../../assets/images/left-chevron.svg')}
            />
          </TouchableOpacity>
          <Image
            style={{width: 32, height: 32, alignSelf: 'flex-end'}}
            source={require('../../assets/images/logocolor.svg')}
          />
        </View>
        <Text
          style={{
            padding: 20,
            fontSize: 24,
            color: '#FFFFFF',
            fontFamily: fontWeight.font_Bold,
          }}>
          Spending limit
        </Text>
        <View
          style={{
            flex: 1,
            backgroundColor: 'white',
            borderTopRightRadius: 30,
            borderTopLeftRadius: 30,
            padding: 20,
          }}>
          <View style={{flexDirection: 'column'}}>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{width: 16, height: 16, alignSelf: 'flex-end'}}
                source={require('../../assets/images/pickupcar.svg')}
              />
              <Text
                style={{
                  marginLeft: 10,
                  fontSize: 14,
                  color: '#222222',
                  fontFamily: fontWeight.font_Bold,
                }}>
                Set a weekly debit card spending limit
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 20,
                alignItems: 'center',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  backgroundColor: '#01D167',
                  width: 40,
                  height: 24,
                  borderRadius: 5,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 13,
                    color: '#FFFFFF',
                    fontFamily: fontWeight.font_Bold,
                  }}>
                  S$
                </Text>
              </View>
              <Text
                style={{
                  marginLeft: 10,
                  fontSize: 24,
                  color: '#222222',
                  fontFamily: fontWeight.font_Bold,
                }}>
                {limit}
              </Text>
            </View>
            <View
              style={{
                height: 1,
                backgroundColor: '#22222266',
                marginTop: 10,
                marginBottom: 10,
              }}
            />
            <Text
              style={{
                fontSize: 13,
                color: '#22222266',
                fontFamily: fontWeight.font_Regular,
              }}>
              Here weekly means the last 7 days - not the calendar week
            </Text>
            <FlatList
              keyboardShouldPersistTaps="handled"
              style={{paddingTop: 10, paddingBottom: 10}}
              keyExtractor={(item, index) => index.toString()}
              data={data}
              numColumns={2}
              columnWrapperStyle={{
                flex: 1,
                justifyContent: 'space-around',
              }}
              renderItem={({item}) => (
                <TouchableOpacity
                  style={{
                    margin: 10,
                  }}
                  onPress={() => {
                    setLimit(item);
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      backgroundColor: '#EFFCF4',
                      width: 114,
                      height: 40,
                      borderRadius: 5,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={{
                        fontSize: 13,
                        color: '#20D167',
                        fontFamily: fontWeight.font_Bold,
                      }}>
                      S$ 5000
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};
export default SpentLimitScreen;
