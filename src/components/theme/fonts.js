const fontWeight = {
  font_Regular: 'AvenirNextLTPro-Regular',
  font_Bold: 'AvenirNextLTPro-Bold',
};
export default fontWeight;
