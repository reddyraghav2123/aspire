import {GET_CARD_INFO} from './index';

export const saveCardInfo = payload => ({
  type: GET_CARD_INFO,
  payload: payload,
});
