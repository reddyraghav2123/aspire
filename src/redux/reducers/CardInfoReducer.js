
import { GET_CARD_INFO } from "../actions";

const initialState = {billingInfo: {}};

function getCardInfoReducer(state = initialState, action) {
  if (action.type === GET_CARD_INFO) {
    return {
      ...state,
      billingInfo: action.payload,
    };
  }
  return state;
}

export default getCardInfoReducer;
