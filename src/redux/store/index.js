import {combineReducers, createStore} from 'redux';
import CardInfoReducer from '../reducers/CardInfoReducer';

const AppReducers = combineReducers({
  cardinfo: CardInfoReducer,
});

const rootReducer = (state, action) => {
  return AppReducers(state, action);
};

let store = createStore(rootReducer);

export default store;
